makebook
========

POSIX-compatible shell script for impressing multiple pdf
pages onto signatures for binding.  Tested and works well on
GNU/Linux and OpenBSD; consciously designed to be
POSIX-compliant and therefore very widely portable.  For
more information, see the website
(https://dgoodmaniii.codeberg.page/makebook).
