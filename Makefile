# +AMDG
#
# This document was begun on 8 June 1200, and it is humbly
# dedicated to St. Wulfric of Haselbury, patron of
# bookbinders, for his prayers, and to the Sacred Heart of
# Jesus, for His mercy.

SHELL=/bin/sh
TROFF=groff
TROFFFLAGS=-man
PREFIX=/usr/local
bindir=$(PREFIX)/bin
DATAROOTDIR=$(PREFIX)/share
DATADIR=$(DATAROOTDIR)
DOCDIR=$(DATAROOTDIR)/doc/makebook
MANDIR=$(DATAROOTDIR)/man
BINFILES=makebook
MANFILES=makebook.1
OTHDOCS=makebook_man.html

install :
	cp $(BINFILES) $(bindir)
	cp $(MANFILES) $(MANDIR)/man1
	test -d $(DOCDIR) || mkdir -p $(DOCDIR)
	cp $(OTHDOCS) $(DOCDIR)

uninstall :
	rm $(bindir)/$(BINFILES)
	rm $(MANDIR)/man1/$(MANFILES)
	rm -R $(DOCDIR)

doc :
	$(TROFF) $(TROFFFLAGS) -Tascii -a makebook.1 > makebook_man.txt;
	$(TROFF) $(TROFFFLAGS) -Thtml makebook.1 > makebook_man.html;
